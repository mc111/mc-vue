package com.mc.web;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/**
 * @author mc
 * @date 2023/3/2
 */
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class},
        scanBasePackages = {"com.mc"})
@MapperScan(value = {"com.mc.*.mapper","com.mc.*.*.mapper"})
public class MCApplication {
    public static void main(String[] args) {



        {
            SpringApplication.run(MCApplication.class, args);
        }
    }
}
