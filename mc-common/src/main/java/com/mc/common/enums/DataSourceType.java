package com.mc.common.enums;

/**
 * 数据源
 * 
 * @author mc
 */
public enum DataSourceType
{
    /**
     * 主库
     */
    MASTER,

    /**
     * 从库
     */
    SLAVE
}
